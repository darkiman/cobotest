import {Platform} from "react-native";

export const sharedStyles = {
    safeView: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    centredRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
    },
    centredColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems:'center'
    },
    flexStartColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems:'center',
        padding: 15
    }
};
