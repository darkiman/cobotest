import React, {useState, useCallback, useMemo} from 'react';
import {
	View,
	SafeAreaView,
	Platform,
	KeyboardAvoidingView,
	TouchableOpacity
} from 'react-native';

import {StyledText} from '../../components/StyledText';

import {sharedStyles} from "../../shared/styles/SharedStyles";
import StyledButton from '../../components/StyledButton';
import themeService from '../../services/ThemeService';
import PhoneInput from '../../components/PhoneInput';
import PasswordInput from '../../components/PasswordInput';
import Modal from 'react-native-modal';

const COLORS = themeService.currentThemeColors;

const isAndroid = Platform.OS === 'android';

const styles = {
	header: {
		fontSize: 24,
		marginTop: 10,
		width: '100%',
        textAlign: isAndroid ? 'left' : 'center'
	},
	loginButton: {
		marginTop: isAndroid ? 20 : 30
	},
	forgotPasswordText: {
		marginTop: 30,
		fontSize: 15,
		color: COLORS.color
	},
	newAccountText: {
		fontSize: 20,
		color: '#222'
	},
	signUpText: {
		marginTop: 8,
		fontSize: 20,
		color: COLORS.color
	},
	splitter: {
		width: '100%',
		height: 1,
		backgroundColor: '#ddd',
		marginTop: isAndroid ? 45 : 60,
		marginBottom: isAndroid ? 40 : 55
	},
	inputs: {
		width: '100%',
		marginTop: 60,
		marginBottom: 10
	},
	passwordInputContainer: {
		marginTop: 15
	},
	modalText: {
		color: 'white',
		fontSize: 18
	}
};


const ProfileView = () => {
	const [phone, setPhone] = useState('');
	const [password, setPassword] = useState('');

	const [isPhoneValid, setIsPhoneValid] = useState(false);
	const [isPasswordValid, setIsPasswordValid] = useState(false);

	const [isLoading, setIsLoading] = useState(false);
	const [modalText, setModalText] = useState('');
	const [isShowModal, setIsShowModal] = useState(false);

	const isFormValid = useMemo(
		() => isPhoneValid && isPasswordValid,
		[isPhoneValid, isPasswordValid]);

	const phoneChange = useCallback((formatted, extracted, isValid) => {
		setPhone(formatted);
		setIsPhoneValid(isValid);
	}, []);

	const passwordChange = useCallback((value, isValid) => {
		setPassword(value);
		setIsPasswordValid(isValid);
	}, []);

	// alert(isFormValid);

	const onLoginPress = () => {
		setIsLoading(true);
		setTimeout(() => {
			if (password === '123' && isPhoneValid) {
				setModalText('Success')
			} else {
				setModalText('Error')
			}
			setIsShowModal(true);
			setIsLoading(false);
		}, 1000)
	};

	return (
		<SafeAreaView style={sharedStyles.safeView}>
			<KeyboardAvoidingView style={sharedStyles.safeView} behavior={(Platform.OS === 'ios') ? 'padding' : null} enabled>
				<View style={{ ...sharedStyles.flexStartColumn }}>
					<StyledText style={styles.header}>Вход</StyledText>
					<View style={styles.inputs}>
						<PhoneInput label={'Телефон *'}
									onChangeText={phoneChange}
						/>
						<PasswordInput containerStyle={styles.passwordInputContainer}
									   label={'Пароль *'}
									   onChangeText={passwordChange}
						/>
					</View>
					<StyledButton containerStyle={styles.loginButton}
								  title={'Войти'}
								  disabled={!isFormValid}
								  loading={isLoading}
								  onPress={onLoginPress}
					/>
					<StyledText style={styles.forgotPasswordText}>Забыл пароль или поменял телефон!</StyledText>
					<View style={styles.splitter} />
					<StyledText style={styles.newAccountText}>Еще нет аккаунта?</StyledText>
					<StyledText style={styles.signUpText}>Зарегистрируйтесь</StyledText>
					<Modal isVisible={isShowModal}>
						<TouchableOpacity onPress={() => setIsShowModal(false)}
										  style={sharedStyles.centredColumn}>
							<StyledText style={styles.modalText}>{modalText}</StyledText>
						</TouchableOpacity>
					</Modal>
				</View>
			</KeyboardAvoidingView>
		</SafeAreaView>
	);
};

export default ProfileView;
