import React, {Component} from 'react';
import BottomTabNavigator from "./AppBottomNavigation";
import ProfileStack from "./ProfileNavigation";
import { NavigationContainer } from '@react-navigation/native';

const AppNavigation = () => {
	return <NavigationContainer>
		{/*<ProfileStack/>*/}
		<BottomTabNavigator />
	</NavigationContainer>;
};

export default AppNavigation;


