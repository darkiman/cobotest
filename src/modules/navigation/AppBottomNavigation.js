import React from 'react';
import {StyledText} from '../../components/StyledText';
import {Icon} from 'react-native-elements';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import NavigationRoutes from "../../constants/NavigationRoutes";
import iconsService from "../../services/IconsService";
import themeService from "../../services/ThemeService";
import ProfileView from '../profile/ProfileView';
import HomeView from '../home/HomeView';
import SearchView from '../search/SearchView';
import BucketView from '../bucket/BucketView';
import MoreView from '../more/MoreView';

const COLORS = themeService.currentThemeColors;
const LABEL_STYLE = {fontSize: 13,  textAlign: 'center', fontFamily: 'Montserrat-Regular'};
const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
	return (
		<Tab.Navigator
			screenOptions={({ route }) => ({
				tabBarIcon: ({ focused, color, size }) => {
					const { name } = route;
					const iconPrefix = iconsService.getIconPrefix();
					let iconName;
					switch(name) {
						case NavigationRoutes.HOME:
							iconName = `${iconPrefix}-home`;
							break;
						case NavigationRoutes.SEARCH:
							iconName = `${iconPrefix}-search`;
							break;
						case NavigationRoutes.BUCKET:
							iconName = `${iconPrefix}-cart`;
							break;
						case NavigationRoutes.PROFILE:
							iconName = `${iconPrefix}-person`;
							break;
						case NavigationRoutes.MORE:
							iconName = `ios-more`;
							break;
					}
					return <Icon type={'ionicon'} name={iconName} size={25} color={ focused ? COLORS.color : COLORS.textLightColor} />;
				},
				tabBarLabel: ({ focused, color, size }) => {
					const { name } = route;
					let text;
					switch(name) {
						case NavigationRoutes.HOME:
							text = 'Главная';
							break;
						case NavigationRoutes.SEARCH:
							text = 'Поиск';
							break;
						case NavigationRoutes.BUCKET:
							text = 'Корзина';
							break;
						case NavigationRoutes.PROFILE:
							text = 'Профиль';
							break;
						case NavigationRoutes.MORE:
							text = 'Еще';
							break;
					}
					return <StyledText style={{...LABEL_STYLE, color: focused ? COLORS.color : COLORS.textColor}}>{text}</StyledText>;
				}
			})}
			tabBarOptions={{
				activeTintColor: COLORS.color,
				inactiveTintColor: 'gray',
			}}
		>
			<Tab.Screen name={NavigationRoutes.HOME} component={HomeView} />
			<Tab.Screen name={NavigationRoutes.SEARCH} component={SearchView} />
			<Tab.Screen name={NavigationRoutes.BUCKET} component={BucketView} />
			<Tab.Screen name={NavigationRoutes.PROFILE} component={ProfileView} />
			<Tab.Screen name={NavigationRoutes.MORE} component={MoreView} />
		</Tab.Navigator>
	);
};
export default BottomTabNavigator;
