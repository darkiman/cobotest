import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProfileView from '../profile/ProfileView';

const Stack = createStackNavigator();

const ProfileStack = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name="Profile" component={ProfileView} />
		</Stack.Navigator>
	);
};
export default ProfileStack;
