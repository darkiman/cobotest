import React, {Component} from 'react';
import {
	View,
	SafeAreaView,
	Platform,
	KeyboardAvoidingView, Text,
} from 'react-native';

import {sharedStyles} from "../../shared/styles/SharedStyles";

export default class MoreView extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<SafeAreaView style={sharedStyles.safeView}>
				<KeyboardAvoidingView style={sharedStyles.safeView} behavior={(Platform.OS === 'ios') ? 'padding' : null} enabled>
					<View style={sharedStyles.centredColumn}>
						<Text>Еще</Text>
					</View>
				</KeyboardAvoidingView>
			</SafeAreaView>
		);
	}
}
