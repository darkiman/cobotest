export default NavigationRoutes = {
	HOME: 'Home',
	SEARCH: 'Search',
	BUCKET: 'Bucket',
	PROFILE: 'Profile',
	MORE: 'More'
}
