import mainColors from "../theme/main/Colors";

class ThemeServiceSingleton {

    constructor() {
        this.currentTheme = 'main';
        this.currentThemeColors = mainColors;
        this.initialized = false;
    }
}

const themeService = new ThemeServiceSingleton();
export default themeService;
