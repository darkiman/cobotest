import React, {useState} from 'react';
import {View, TextInput} from "react-native";
import StyledText from './StyledText';
import { labelStyle, containerStyle, inputContainerStyle, inputStyle } from './Styles';

export const checkMarkStyle = {
	position: 'absolute',
	right: 15,
	top: 25
};

export const MAX_LENGTH = 30;

export const PasswordInput = (props) => {
	const [isValid, setIsValid] = useState(false);

	return <View style={{...containerStyle, ...props.containerStyle}}>
		<StyledText style={labelStyle}>{props.label}</StyledText>
		<View style={inputContainerStyle}>
			<TextInput
				style={inputStyle}
				secureTextEntry={true}
				maxLength={MAX_LENGTH}
				onChangeText={(val) => {
					const valid = val && !!val.length;
					setIsValid(valid);
					if (props.onChangeText) {
						props.onChangeText(val, valid);
					}
				}}
			/>
		</View>
	</View>;
};

export default PasswordInput;
