import React from 'react';
import {Text} from 'react-native';

export const styles = {
	fontFamily: 'Montserrat-Bold'
};

export const StyledText = (props) => {
	return <Text style={{...styles, ...props.style}}>{props.children}</Text>;
};

export default StyledText;
