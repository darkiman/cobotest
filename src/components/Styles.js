const isAndroid = Platform.OS === 'android';

export const containerStyle = {
	borderColor: '#ddd',
	width: '100%',
	borderWidth: 1,
	borderRadius: 6,
	paddingTop: 5,
	paddingBottom: 0,
	paddingRight: 20,
	paddingLeft: 20,
	position: 'relative'
};

export const labelStyle = {
	fontFamily: 'Montserrat-Regular',
	color: '#aaa',
	fontSize: 14,
	marginTop: isAndroid ? 4 : 7,
	marginBottom: isAndroid ? -10 : 0
};

export const inputContainerStyle = {
	marginTop: isAndroid ? 0 : 5,
	marginBottom: isAndroid ? 0 : 12
};

export const inputStyle = {
	marginTop: 0,
	fontSize: 18,
	fontFamily: 'Montserrat-Bold'
};
