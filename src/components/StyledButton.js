import React from 'react';
import {Button} from 'react-native-elements';
import themeService from '../services/ThemeService';


const COLORS = themeService.currentThemeColors;
const isAndroid = Platform.OS === 'android';

export const buttonStyle = {
	backgroundColor: COLORS.color,
	height: isAndroid ? 60 : 70
};

export const containerStyle = {
	backgroundColor: COLORS.color,
	width: '100%'
};

export const titleStyle = {
	fontFamily: 'Montserrat-Bold'
};

export const disabledStyle = {
	opacity: 0.3
};

export const StyledButton = (props) => {
	return <Button buttonStyle={{...buttonStyle, ...props.buttonStyle}}
				   containerStyle={{...containerStyle, ...props.containerStyle}}
				   titleStyle={{...titleStyle, ...props.titleStyle}}
				   icon={props.icon}
				   title={props.title}
				   disabled={props.disabled}
				   disabledStyle={disabledStyle}
				   loading={props.loading}
				   onPress={props.onPress}
	>
		{props.children}
	</Button>;
};

export default StyledButton;
