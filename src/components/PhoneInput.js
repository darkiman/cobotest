import React, {useState} from 'react';
import {View} from "react-native";
import TextInputMask from 'react-native-text-input-mask';
import StyledText from './StyledText';
import { SvgXml } from 'react-native-svg';
import { checkMarkSvg } from '../../assets/icons/checkMark';
import { labelStyle, containerStyle, inputContainerStyle, inputStyle } from './Styles';

export const checkMarkStyle = {
	position: 'absolute',
	right: 20,
	top: 25
};

const phoneRegex = /^([0-9]){10}$/;

export const PhoneInput = (props) => {
	const [isValid, setIsValid] = useState(false);

	return <View style={containerStyle}>
		<StyledText style={labelStyle}>{props.label}</StyledText>
		<View style={inputContainerStyle}>
			<TextInputMask
				style={inputStyle}
				onChangeText={(formatted, extracted) => {
					const valid = !!extracted.match(phoneRegex);
					setIsValid(valid);
					if (props.onChangeText) {
						props.onChangeText(formatted, extracted, valid);
					}
				}}
				mask={"+7 ([000]) [000]-[00]-[00]"}
			/>
		</View>
		{
			isValid ? <SvgXml style={checkMarkStyle} xml={checkMarkSvg} width={20} height={20} /> : <></>
		}
	</View>;
};

export default PhoneInput;
