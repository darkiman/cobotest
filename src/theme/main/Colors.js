const blueColors = {
    color: '#00c0ae',
    textLightColor: '#808080',
    textColor: '#555'
};

export default blueColors;
